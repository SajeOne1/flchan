//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// File: flchan.c
// Author: Shane "SajeOne" Brown
// Date: 2018-10-27
// Description: Uses flchan spritesheet to dance to mpd fifo output
//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

// STD
#include <stdio.h>
#include <string.h>
#include <time.h>

// FIFO CHECK
#include <unistd.h>
#include <fcntl.h>
#include <sys/select.h>

// SPOTIFY CHECK
#include <playerctl/playerctl.h>

// SDL
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

// FLCHAN
#include "flchan_sprite.xpm"

// FLCHAN SPRITE PARAMS
#define CELL_WIDTH 110
#define CELL_HEIGHT 128
#define NUM_ROWS 10
#define NUM_COLS 8

typedef char byte;

// Locking up main thread a bit :/, needed feature will have to look into pthreads
byte is_spotify_playing(){ 
    byte is_playing = 0;
    PlayerctlPlayer* player = playerctl_player_new("spotify", NULL);

    if(player == NULL){
        return is_playing;
    }

    gchar* status;
    g_object_get(player, "status", &status, NULL);

    if(status != NULL){
        if(strcmp(status, "Playing") == 0){
            is_playing++;
        }

        g_free(status);
    }

    return is_playing;
}

byte is_music_playing(){
    fd_set fds;
    struct timeval timeout;
    byte is_playing = 0;


    int ptr = open("/tmp/mpd.fifo", O_RDONLY);


    FD_ZERO(&fds);
    FD_SET(ptr, &fds);

    timeout.tv_sec = 0;
    timeout.tv_usec = 100;

    if(select(ptr+1, &fds, 0, 0, &timeout) == 1){
        is_playing = 1;
    }
    close(ptr);
    return is_playing;
}

SDL_Texture* loadTexture(SDL_Renderer* renderer){
	SDL_Texture* texture = NULL;

	//SDL_Surface* img_surface = IMG_Load(path); // Better quality but not portable :/
	SDL_Surface* img_surface = IMG_ReadXPMFromArray(flchan_sprite_xpm);

	if(img_surface == NULL){
		printf("Could not load image from path");
		return NULL;
	}

	texture = SDL_CreateTextureFromSurface(renderer, img_surface);
	if(texture == NULL){
		printf("Could not create texture from image surface\n");
		return NULL;
	}

	SDL_FreeSurface(img_surface);

	return texture;
}

// Slices spritemap into sections based on sprite dimensions
void prepare_rect(SDL_Rect* rect, int size, int width, int height, int cols){
	int incy = 0;
	int incx = 0;
	for(int i = 0; i < size; i++){
		rect[i].x = incx * width;
		rect[i].y = incy * height;
		rect[i].w = width;
		rect[i].h = height;
		incx++;

		if(rect[i].x >= width * (cols-1)){
			incy++;	
			incx = 0;
		}
	}
}

void render_sprite(SDL_Renderer* renderer, SDL_Texture* texture, int x, int y, int width, int height, SDL_Rect* clip){
	// RENDER CODE
	SDL_Rect quad = {x, y, 110, 128};
	if(clip != NULL){
		quad.w = clip->w;	
		quad.h = clip->h;
	}

	SDL_RenderCopy(renderer, texture, clip, &quad);
}

int main(int argc, char* argv[]){
	SDL_Window* window = NULL;
	SDL_Renderer* renderer = NULL;
	SDL_Event e;
	SDL_Rect sprites[NUM_COLS*NUM_ROWS];
	srand(time(NULL));

	if(SDL_Init(SDL_INIT_VIDEO) < 0){
		printf("SDL_INIT FAILED\n");
		return 1;
	}

	window = SDL_CreateWindow("FL Chan", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 110, 128, SDL_WINDOW_SHOWN);
	if(window == NULL){
		printf("Failed to create SDL window\n");
		return 1;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if(renderer == NULL){
		printf("Could not create renderer\n");
	}


	int img_flags = IMG_INIT_PNG;
	if(!(IMG_Init(img_flags) & img_flags)){
		printf("Could not load SDL_Image\n");
		return 1;
	}

	SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" );

	SDL_Texture* main_texture = loadTexture(renderer);

	prepare_rect(sprites, NUM_ROWS*NUM_COLS, CELL_WIDTH, CELL_HEIGHT, NUM_COLS);

	int sprite_count = 0; // index to enter spritesheet
	int sprite_max = 7; // index to exit spritesheet
	int sprite_repeat = 0; // Random buffer for how many times to repeat the current sprite sheet
	int dance_num = 0; // Random buffer for which sprite row to use for dance
	int quit = 0; // If SDL sends quit set to TRUE to exit main loop
	int window_width, window_height = 0; // Used in GetWindowSize() to center flchan
	int color_code = 0;
	int options[] = {
		0xFF, 0x59, 0x59, // Pink/Red
		0x0C, 0x12, 0x1E, // Navy 
		0x00, 0x00, 0x00, // Black
		0xFF, 0xFF, 0xFF  // White
	};

	while(!quit){
		while(SDL_PollEvent(&e) != 0){
			if(e.type == SDL_QUIT){
				quit = 1;
			}else if(e.type == SDL_KEYDOWN){
				switch(e.key.keysym.sym){
					case SDLK_1:
						color_code = 0;
						break;
					case SDLK_2:
						color_code = 3;
						break;
					case SDLK_3:
						color_code = 6;
						break;
					case SDLK_4:
						color_code = 9;
						break;
				}
			}

		}


		
		// CLEAR SCREEN
		SDL_SetRenderDrawColor(renderer, options[color_code], options[color_code+1], options[color_code+2], 0x00);
		SDL_RenderClear(renderer);

		SDL_GetWindowSize(window, &window_width, &window_height);

		// Render current sprite sheet
		render_sprite(renderer, main_texture, (window_width/2)-(CELL_WIDTH/2), (window_height/2)-(CELL_HEIGHT/2), CELL_WIDTH, CELL_HEIGHT, &(sprites[sprite_count]));

		// Reset sprite sheet when it meets the end
		if(sprite_count >= sprite_max){ // END OF DANCE
			if(is_spotify_playing() || is_music_playing()){
				if(!sprite_repeat){
					dance_num = (rand() % 9) +1; // row[1-9]: exclude 0, non-dance
					sprite_count = (dance_num * NUM_COLS);
					sprite_max = (dance_num * NUM_COLS) + (NUM_COLS-1);

					sprite_repeat = (rand() % 4)+1;
				}else{
					sprite_count = (dance_num*NUM_COLS);
					sprite_repeat--;
				}
			}else{
				sprite_count = 0;
				sprite_max = 7;
				sprite_repeat = 0;
				
			}
		}else{ // NEXT DANCE SPRITE
			sprite_count++;
		}

		// RENDER FINAL
		SDL_RenderPresent(renderer);
		SDL_Delay(200);
	}

	SDL_DestroyTexture(main_texture);
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	IMG_Quit();
	SDL_Quit();


	return 0;
}
